"""Copied from: https://gitlab.com/Jaco-Assistant/texnom/-/blob/master/texnom/basic_normalizer.py"""

import re
import unicodedata
from typing import List

# ==================================================================================================


class Normalizer:
    """A simple normalizer that is language agnostic and has no external dependencies"""

    def __init__(self, allowed_chars: List[str]) -> None:
        self.allowed_chars = allowed_chars

        # Regex patterns, see www.regexr.com for good explanation
        self.multi_space_pattern = re.compile(r"\s+")

    # ==============================================================================================

    def clean_sentence(self, sentence: str) -> str:
        """Normalize the given sentence. Return a tuple (cleaned sentence)"""

        # Convert to lower and replace additional spaces
        sentence = sentence.lower()
        sentence = self.remove_duplicate_whitespaces(sentence)

        words = sentence.split()
        cleaned_words = []
        for word in words:
            cleaned_word = self.clean_word(word)
            cleaned_words.append(cleaned_word)
        sentence = " ".join(cleaned_words)
        sentence = sentence.lower()

        sentence = self.remove_duplicate_whitespaces(sentence)
        return sentence

    # ==============================================================================================

    def clean_word(self, word: str) -> str:
        """Clean the given word by removing punctuation, converting special characters and dropping
        all that are not especially whitelisted"""

        # Remove symbols and convert special characters to their base form
        chars = []
        for c in word:
            if c not in self.allowed_chars:
                c = self.remove_symbol(c)
                if c != "":
                    c = Normalizer.rmdiacritics(c)
            chars.append(c)
        word = "".join(chars)

        # Drop all chars that are not explicitly allowed
        chars = []
        for c in word:
            if c in self.allowed_chars:
                chars.append(c)
        word = "".join(chars)

        return word

    # ==============================================================================================

    def remove_duplicate_whitespaces(self, sentence: str) -> str:
        # Remove duplicate whitespaces again, we may have added some with the above steps
        sentence = re.sub(self.multi_space_pattern, " ", sentence)
        sentence = sentence.strip()
        return sentence

    # ==============================================================================================

    @staticmethod
    def remove_symbol(char: str) -> str:
        """Replace any other markers, symbols, punctuations with a space, keeping diacritics
        See: https://www.unicode.org/Public/5.1.0/ucd/UCD.html#Property_Values"""

        uc = unicodedata.normalize("NFKC", char)
        char = " " if unicodedata.category(uc)[0] in "MSP" else uc
        return char

    # ==============================================================================================

    @staticmethod
    def rmdiacritics(char: str) -> str:
        """
        Return the base character of char, by "removing" any
        diacritics like accents or curls and strokes and the like.
        From: https://stackoverflow.com/a/15547803
        """

        desc = unicodedata.name(char)
        cutoff = desc.find(" WITH ")
        if cutoff != -1:
            desc = desc[:cutoff]
            try:
                char = unicodedata.lookup(desc)
            except KeyError:
                pass  # removing "WITH ..." produced an invalid name
        return char
