## intent:get_riddle
- i want a riddle!
- a new riddle please.

## lookup:riddle_answers
`a comment` \
`with multiple lines`
riddle_answers.txt

## intent:check_riddle
- the answer is: [snow](riddle_answers)
- `another comment`
- is the solution [doll](riddle_answers?test)?
