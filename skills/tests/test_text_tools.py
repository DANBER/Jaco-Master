import os
import sys

filepath = os.path.dirname(os.path.realpath(__file__)) + "/"
sys.path.append(filepath + "../")
import text_tools  # noqa: E402 pylint: disable=wrong-import-position

# ==================================================================================================


def test_clean_text_for_stt() -> None:
    text = "Löwenzahn"
    language = "de"
    text = text_tools.clean_text_for_stt(text, language)
    assert text == "löwenzahn"

    text = "I'dlik3much$$"
    language = "en"
    text = text_tools.clean_text_for_stt(text, language)
    assert text == "i'dlikmuch"

    text = "Me pica el bagre"
    language = "es"
    text = text_tools.clean_text_for_stt(text, language)
    assert text == "me pica el bagre"

    text = "Liberté, Égalité, Fraternité"
    language = "fr"
    text = text_tools.clean_text_for_stt(text, language)
    assert text == "liberté égalité fraternité"
