#! /bin/bash

ARCHITECTURE="arm64"
NAMESPACE=${NAMESPACE:="jaco-assistant"}
BRANCH=${BRANCH:="release"}

# Login into registry first
echo "Login into gitlab registry:"
docker login registry.gitlab.com

# echo ""
# echo "Uploading master base image ..."
# LINK="registry.gitlab.com/${NAMESPACE}/jaco-master/master_base_image_${ARCHITECTURE}:${BRANCH}"
# docker tag "master_base_image_${ARCHITECTURE}" "${LINK}"
# docker push "${LINK}"

# echo ""
# echo "Uploading mqtt-broker image ..."
# LINK="registry.gitlab.com/${NAMESPACE}/jaco-master/mqtt_broker_${ARCHITECTURE}:${BRANCH}"
# docker tag "mqtt_broker_${ARCHITECTURE}" "${LINK}"
# docker push "${LINK}"

echo ""
echo "Uploading slu-parser image ..."
LINK="registry.gitlab.com/${NAMESPACE}/jaco-master/slu_parser_${ARCHITECTURE}:${BRANCH}"
docker tag "slu_parser_${ARCHITECTURE}" "${LINK}"
docker push "${LINK}"

echo ""
echo "Uploading duckling image ..."
LINK="registry.gitlab.com/${NAMESPACE}/jaco-master/duckling_${ARCHITECTURE}:${BRANCH}"
docker tag "duckling_${ARCHITECTURE}" "${LINK}"
docker push "${LINK}"

# echo ""
# echo "Uploading text-to-speech image ..."
# LINK="registry.gitlab.com/${NAMESPACE}/jaco-master/text_to_speech_${ARCHITECTURE}:${BRANCH}"
# docker tag "text_to_speech_${ARCHITECTURE}" "${LINK}"
# docker push "${LINK}"

echo ""
echo "FINISHED UPLOADS"
