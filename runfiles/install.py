import argparse
import os
import platform
import subprocess
import sys
import time

# Load utils from jacolib without installing it locally with all dependencies
file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
sys.path.append(file_path + "../jacolib/jacolib/")
import utils  # noqa: E402 pylint: disable=wrong-import-position

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
utils.set_repo_path(file_path + "../")


# ==================================================================================================


def pull_and_retag_image(name, branch, namespace):
    """Download image from repository registry and retag it"""

    link = "registry.gitlab.com/{}/jaco-master/{}:{}"
    link = link.format(namespace, name, branch)
    cmd = "docker pull {} && docker tag {} {}"
    cmd = cmd.format(link, link, name)
    subprocess.call(["/bin/bash", "-c", cmd])


# ==================================================================================================


def download_module_containers(branch="master", namespace="jaco-assistant"):
    """Download prebuilt container images of the modules"""

    architecture = utils.load_architecture()
    start_time = time.time()

    print("\nDownloading master base image ...")
    name = "master_base_image_{}".format(architecture)
    pull_and_retag_image(name, branch, namespace)

    print("\nDownloading mqtt-broker image ...")
    name = "mqtt_broker_{}".format(architecture)
    pull_and_retag_image(name, branch, namespace)

    print("\nDownloading slu-parser image ...")
    name = "slu_parser_{}".format(architecture)
    pull_and_retag_image(name, branch, namespace)

    print("\nDownloading duckling image ...")
    name = "duckling_{}".format(architecture)
    pull_and_retag_image(name, branch, namespace)

    print("\nDownloading text-to-speech image ...")
    name = "text_to_speech_{}".format(architecture)
    pull_and_retag_image(name, branch, namespace)

    print("\nDownloading portainer image ...")
    cmd = "docker pull portainer/portainer-ce"
    subprocess.call(["/bin/bash", "-c", cmd])

    end_time = time.time()
    msg = "\nDownloading the module images took {} hours\n"
    print(msg.format(utils.seconds_to_hours(end_time - start_time)))


# ==================================================================================================


def build_image(cf_path, tag, architecture):
    """Build squashed container image"""

    docker_platform = "linux/" + architecture

    # Experimental features are required for squashing and buildx for the multiplatform builds
    cmd = "docker buildx build --platform={} --no-cache --squash -t {} - < {}"

    cmd = cmd.format(docker_platform, tag, cf_path)
    print("Running: {}".format(cmd))
    subprocess.call(["/bin/bash", "-c", cmd])


# ==================================================================================================


def build_module_containers(architecture):
    """Build container images of the modules"""

    if architecture is None and platform.machine() in ["x86_64"]:
        architecture = "amd64"
    if architecture is None and platform.machine() in ["aarch64"]:
        architecture = "arm64"
    start_time = time.time()

    print("\nBuilding master base image ...")
    cf_path = file_path + "Containerfile_MasterBase_{}".format(architecture)
    tag = "master_base_image_{}".format(architecture)
    build_image(cf_path, tag, architecture)

    print("\nBuilding mqtt-broker image ...")
    cf_path = file_path + "../mqtt-broker/Containerfile_{}".format(architecture)
    tag = "mqtt_broker_{}".format(architecture)
    build_image(cf_path, tag, architecture)

    print("\nBuilding slu-parser image ...")
    cf_path = file_path + "../slu-parser/Containerfile_{}".format(architecture)
    tag = "slu_parser_{}".format(architecture)
    build_image(cf_path, tag, architecture)

    print("\nBuilding duckling image ...")
    cf_path = file_path + "../slu-parser/Containerfile_Duckling_{}".format(architecture)
    tag = "duckling_{}".format(architecture)
    build_image(cf_path, tag, architecture)

    print("\nBuilding text-to-speech image ...")
    cf_path = file_path + "../text-to-speech/Containerfile_{}".format(architecture)
    tag = "text_to_speech_{}".format(architecture)
    build_image(cf_path, tag, architecture)

    end_time = time.time()
    msg = "\nBuilding the module images took {} hours\n"
    print(msg.format(utils.seconds_to_hours(end_time - start_time)))


# ==================================================================================================


def train_slu_models():
    """Generate the slu models"""

    architecture = utils.load_architecture()
    start_time = time.time()

    print("\nCollecting and extending skill data ...")
    cmd = "docker run --network host --rm \
      --volume `pwd`/skills/:/Jaco-Master/skills/ \
      --volume `pwd`/slu-parser/:/Jaco-Master/slu-parser/:ro \
      --volume `pwd`/userdata/:/Jaco-Master/userdata/:ro \
      --volume `pwd`/jacolib/:/Jaco-Master/jacolib/:ro \
      -it master_base_image_{} /bin/bash -c '\
        python3 /Jaco-Master/skills/collect_data.py &&  \
        python3 /Jaco-Master/skills/map_intents_slots.py && \
        python3 /Jaco-Master/skills/generate_data.py'"
    cmd = cmd.format(architecture)
    subprocess.call(["/bin/bash", "-c", cmd])

    print("\nBuilding slu model ...")
    cmd = "docker run --network host --rm \
      --volume `pwd`/slu-parser/:/Jaco-Master/slu-parser/:ro \
      --volume `pwd`/slu-parser/moduldata/:/Jaco-Master/slu-parser/moduldata/ \
      --volume `pwd`/skills/generated/nlu/:/Jaco-Master/slu-parser/moduldata/nlu/:ro \
      --volume `pwd`/userdata/:/Jaco-Master/userdata/:ro \
      --volume `pwd`/jacolib/:/Jaco-Master/jacolib/:ro \
      -it slu_parser_{} python3 /Jaco-Master/slu-parser/build_model.py"
    cmd = cmd.format(architecture)
    subprocess.call(["/bin/bash", "-c", cmd])

    end_time = time.time()
    msg = "\nBuilding the slu models took {} hours\n"
    print(msg.format(utils.seconds_to_hours(end_time - start_time)))


# ==================================================================================================


def build_skill_containers(architecture):
    """Build containers for skills having a Containerfile"""

    if architecture is None and platform.machine() in ["x86_64"]:
        architecture = "amd64"
    if architecture is None and platform.machine() in ["aarch64"]:
        architecture = "arm64"
    docker_platform = "linux/" + architecture
    start_time = time.time()

    skills_with_paths = utils.get_skills_with_paths()
    for skill, skill_path in skills_with_paths:
        if skill == "Skill-Riddles":
            # Don't build the demo Containerfile
            continue

        skill_files = os.listdir(skill_path)
        cf_name = "Containerfile_{}".format(architecture)

        if cf_name in skill_files:
            print("\nBuilding skill {} ...".format(skill))
            tag = "skill_" + utils.skill_to_prefix(skill) + "_" + architecture
            cmd = "docker buildx build --platform={} -t {} - < {}"
            cmd = cmd.format(docker_platform, tag, os.path.join(skill_path, cf_name))
            subprocess.call(["/bin/bash", "-c", cmd])
        elif any("Containerfile" in f for f in skill_files):
            # Skip skill because it has a Containerfile which is not matching the used architecture
            msg = "Skipping {} because it has no valid Containerfile"
            print(msg.format(skill))

    end_time = time.time()
    msg = "\nBuilding extra skill images took {} hours\n"
    print(msg.format(utils.seconds_to_hours(end_time - start_time)))


# ==================================================================================================


def reset_skill_data():
    """Delete all content from skilldata directories"""

    skills_with_paths = utils.get_skills_with_paths()
    for skill, skill_path in skills_with_paths:
        skill_files = os.listdir(skill_path)

        if "skilldata" in skill_files:
            utils.empty_with_ignore(os.path.join(skill_path, "skilldata"))
            print("Deleted data of {}".format(skill))

    handle_topic_keys("distribute")


# ==================================================================================================


def download_stt_model():
    """Download stt model checkpoint"""

    architecture = utils.load_architecture()
    cmd = "docker run --network host --rm \
      --volume `pwd`/slu-parser/:/Jaco-Master/slu-parser/:ro \
      --volume `pwd`/slu-parser/moduldata/:/Jaco-Master/slu-parser/moduldata/ \
      --volume `pwd`/userdata/:/Jaco-Master/userdata/:ro \
      --volume `pwd`/jacolib/:/Jaco-Master/jacolib/:ro \
      -it slu_parser_{} \
        python3 /Jaco-Master/slu-parser/download_stt_model.py"
    cmd = cmd.format(architecture)
    subprocess.call(["/bin/bash", "-c", cmd])


# ==================================================================================================


def download_or_update_skills():
    """Download skills or try to update them if already existing"""

    print("\nUpdating skills ...")
    print("If a repository needs authentication, you can skip it by pressing enter\n")

    skills_with_paths = utils.get_skills_with_paths()
    for s, p in skills_with_paths:
        print("Updating {} ...".format(s))
        cmd = "cd {} && git pull"
        cmd = cmd.format(p)
        subprocess.call(["/bin/bash", "-c", cmd])
        print("")

    # Load selected skills from file
    skills = []
    path = file_path + "../userdata/my_selected_skills.txt"
    if os.path.exists(path):
        with open(path, "r", encoding="utf-8") as file:
            skills = file.readlines()

    # Download the dialogs skills if it's not existing already
    skills.append("https://gitlab.com/Jaco-Assistant/Skill-Dialogs")

    print("\nDownloading new skills ...")
    # I didn't find a way to suppress the error messages, without silencing the standard status
    #  messages if the repository doesn't exist and gets cloned
    # Maybe you could send an api request to get the repository's directory name, (like in the
    #  SkillStore), but then it wouldn't be possible to use all git urls (only github and gitlab)
    # In contrast to the store, here the users should be able to use custom or private repositories
    print("Please ignore the 'destination path already exists' error messages\n")

    for s in skills:
        if s.strip() != "":
            cmd = "cd {} && git clone {}"
            cmd = cmd.format(file_path + "../skills/skills/", s)
            subprocess.call(["/bin/bash", "-c", cmd])
            print("")

    # Generate new keys for all skills, because its easier than filtering the new skills
    handle_topic_keys("gen_for_skills")
    handle_topic_keys("distribute")


# ==================================================================================================


def generate_modules_compose_file():
    """Generate modules compose file"""

    path = file_path + "modules-compose-template.yml"
    with open(path, "r", encoding="utf-8") as file:
        modules_compose = file.read()

    architecture = utils.load_architecture()
    modules_compose = modules_compose.replace("{{architecture}}", architecture)

    path = file_path + "../userdata/start-modules-compose.yml"
    with open(path, "w+", encoding="utf-8") as file:
        file.write(modules_compose)


# ==================================================================================================


def generate_skills_compose_file():
    """Generate skills compose files"""

    path = file_path + "skill-compose-template.yml"
    with open(path, "r", encoding="utf-8") as file:
        skill_template = file.read()

    architecture = utils.load_architecture()
    skills_compose = []

    skills_with_paths = utils.get_skills_with_paths()
    for skill, skill_path in skills_with_paths:

        skill_config = utils.load_skill_config(skill_path)
        if not skill_config["system"]["has_action"]:
            # Skip skill because it has no action
            continue

        skill_files = os.listdir(skill_path)
        action_name = None
        for f in skill_files:
            if f.startswith("action_") or f.startswith("action-"):
                action_name = f
                break

        if action_name is not None:
            skill_cmd = skill_template
            skill_prefix = "skill_" + utils.skill_to_prefix(skill)
            extra_flags = skill_config["system"]["extra_container_flags"]

            skill_cmd = skill_cmd.replace("{{skill_tag}}", skill_prefix)
            skill_cmd = skill_cmd.replace("{{skill_name}}", skill)
            skill_cmd = skill_cmd.replace("{{action_name}}", action_name)
            skill_cmd = skill_cmd.replace("{{extra_flags}}", extra_flags)

            cf_name = "Containerfile_{}".format(architecture)
            if cf_name in skill_files and skill != "Skill-Riddles":
                # Skill has it's own container, except the demo skill
                skill_img = skill_prefix + "_" + architecture
                skill_cmd = skill_cmd.replace("{{skill_image}}", skill_img)
            elif (
                any("Containerfile" in f for f in skill_files)
                and skill != "Skill-Riddles"
            ):
                # Skip such skills
                msg = "Skill {} was excluded from your runnable skills, "
                msg += "because it has no Containerfile matching the architecture"
                print(msg.format(skill))
                continue
            else:
                skill_img = "master_base_image" + "_" + architecture
                skill_cmd = skill_cmd.replace("{{skill_image}}", skill_img)

            skills_compose.append(skill_cmd)

    compose_header = 'version: "2"\nservices:\n'
    skills_compose = compose_header + "".join(skills_compose)

    path = file_path + "../userdata/start-skills-compose.yml"
    with open(path, "w+", encoding="utf-8") as file:
        file.write(skills_compose)


# ==================================================================================================


def handle_topic_keys(mode):
    """Generate password keys to encrypt the payload of all mqtt topics
    or distribute them to the skills"""

    if mode == "gen_for_modules":
        arg = "--generate_module_keys"
    elif mode == "gen_for_skills":
        arg = "--generate_skill_keys"
    elif mode == "distribute":
        arg = "--distribute_keys"
    else:
        raise ValueError
    architecture = utils.load_architecture()

    cmd = "docker run --network host --rm \
      --volume `pwd`/runfiles/:/Jaco-Master/runfiles/:ro \
      --volume `pwd`/skills/:/Jaco-Master/skills/ \
      --volume `pwd`/userdata/:/Jaco-Master/userdata/ \
      --volume `pwd`/jacolib/:/Jaco-Master/jacolib/:ro \
      -it master_base_image_{} /bin/bash -c '\
        python3 /Jaco-Master/runfiles/handle_topic_keys.py {}'"
    cmd = cmd.format(architecture, arg)
    subprocess.call(["/bin/bash", "-c", cmd])


# ==================================================================================================


def create_skills_file():
    """Create the my_selected_skill.txt file if it doesn't exist and add the riddles demo skill"""

    path = file_path + "../userdata/my_selected_skills.txt"
    if not os.path.exists(path):
        with open(path, "w+", encoding="utf-8") as file:
            link = "https://gitlab.com/Jaco-Assistant/Skill-Riddles"
            file.write(link)


# ==================================================================================================


def main():
    parser = argparse.ArgumentParser(description="Install Jaco-Master modules")
    parser.add_argument(
        "--install_modules", action="store_true", help="Install or update all modules"
    )
    parser.add_argument(
        "--build_modules", action="store_true", help="Build modules yourself"
    )
    parser.add_argument(
        "--download_modules",
        action="store_true",
        help="Download prebuilt module images",
    )
    parser.add_argument(
        "--download_stt_model", action="store_true", help="Download stt model"
    )
    parser.add_argument(
        "--update_skills",
        action="store_true",
        help="Download new skills or update already installed",
    )
    parser.add_argument(
        "--reset_skills", action="store_true", help="Delete skilldata of all skills"
    )
    parser.add_argument("--rebuild_slu", action="store_true", help="Rebuild slu models")
    parser.add_argument(
        "--new_topic_keys",
        action="store_true",
        help="Generate and distribute new topic encryption keys",
    )
    parser.add_argument(
        "--new_compose", action="store_true", help="Rebuild start compose file"
    )
    parser.add_argument(
        "--namespace",
        action="store",
        default="jaco-assistant",
        help="Set namespace used for downloading images",
    )
    parser.add_argument(
        "--branch",
        action="store",
        default="master",
        help="Set branch used for downloading images",
    )
    parser.add_argument(
        "--architecture",
        action="store",
        choices=["amd64", "arm64"],
        help="Set architecture used for building images",
    )
    args = parser.parse_args()

    if not any(list(vars(args).values())):
        print("Run with '--help' or '-h' for argument explanations")

    if args.install_modules:
        create_skills_file()
        download_module_containers(args.branch, args.namespace)
        download_stt_model()
        handle_topic_keys("gen_for_modules")

    if args.build_modules:
        build_module_containers(args.architecture)

    if args.download_modules:
        download_module_containers(args.branch, args.namespace)

    if args.download_stt_model:
        download_stt_model()

    if args.update_skills:
        download_or_update_skills()
        build_skill_containers(args.architecture)
        handle_topic_keys("gen_for_skills")
        handle_topic_keys("distribute")
        generate_modules_compose_file()
        generate_skills_compose_file()
        train_slu_models()

    if args.reset_skills:
        reset_skill_data()

    if args.rebuild_slu:
        train_slu_models()

    if args.new_topic_keys:
        handle_topic_keys("gen_for_modules")
        handle_topic_keys("gen_for_skills")
        handle_topic_keys("distribute")

    if args.new_compose:
        generate_modules_compose_file()
        generate_skills_compose_file()


# ==================================================================================================

if __name__ == "__main__":
    main()
