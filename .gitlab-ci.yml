# Pipelines to test code and build container images

stages:
  # Install testing tools into extra container to speed up later testing steps
  - container_testing
  - test
  - container_base
  - container_modules
  - release

# ==================================================================================================
# ===== Stage: Test-Container ======================================================================

# Use anchors to reuse parts for the different steps
.stage_container_anchor: &container_anchor
  interruptible: true
  image: "quay.io/buildah/stable"
  variables:
    # Use vfs with buildah. Docker offers overlayfs as a default, but buildah
    # cannot stack overlayfs on top of another overlayfs filesystem.
    STORAGE_DRIVER: "vfs"
    BUILDAH_FORMAT: "oci"
  before_script:
    # Print out program versions for debugging
    - buildah version
    - buildah login --username "${CI_REGISTRY_USER}" --password "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"

testing_image:
  <<: *container_anchor
  stage: container_testing
  variables:
    # Set base image variables here because it didn't work to set them in the anchor
    IMAGE_NAME: "testing_jaco_mtr"
    IMAGE_NAME_FULL: "${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
    IMAGE_FILE: "tests/Containerfile"
  script:
    # Build image, using layers=false squashes only new layers,
    # but this reduces space usage while building and is much faster
    - echo "buildah bud --layers=false --squash -f ${IMAGE_FILE} -t ${IMAGE_NAME} ."
    - buildah bud --layers=false --squash -f ${IMAGE_FILE} -t ${IMAGE_NAME} .
    - buildah tag ${IMAGE_NAME} ${IMAGE_NAME_FULL}
    - buildah images
    # Push images
    - buildah push ${IMAGE_NAME_FULL}
  only:
    refs:
      # Run only for important branches or if the branch name starts with the 'containerbuild' keyword,
      # else every new branch would start to build this container
      - develop
      - release
      - /^containerbuild/

# ==================================================================================================
# ===== Stage: Test ================================================================================

# Use anchors to reuse parts for the different steps
.stage_test_anchor: &test_anchor
  variables:
    IMAGE_NAME: "testing_jaco_mtr"
    # Use the main repository's container image by default except for specific branch names
    IMAGE_NAME_FULL: "registry.gitlab.com/jaco-assistant/jaco-master/${IMAGE_NAME}:master"
  rules:
    - if: $CI_COMMIT_REF_SLUG == "develop" || $CI_COMMIT_REF_SLUG == "release" || $CI_COMMIT_REF_SLUG =~ /^containerbuild/
      variables:
        IMAGE_NAME_FULL: "${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
    - when: always
  image: ${IMAGE_NAME_FULL}

linting:
  <<: *test_anchor
  stage: test
  script:
    - isort --check-only --diff .
    - black --check --diff .
    - npx /usr/app/node_modules/prettylint/bin/cli.js \
      $(find . -type f \( -name '*.json' -o -name '*.md' -o -name '*.yaml' \) ! -path './.*/*')
    - pylint --disable=import-error $(find . -type f -name "*.py" -not -path "./extras/*" -not -path "./skills/skills/*")
    - flake8 --exclude skills/skills/ .
    # Next step is only required for gitlab's pipeline
    - ls -d -1 "$PWD"/**/* && rm -r jacolib/ && git clone https://gitlab.com/Jaco-Assistant/jacolib && pip3 install -e jacolib/
    - mypy --exclude tests/ .
    - shellcheck $(find . -type f \( -name '*.sh' -o -name '*.bash' \) ! -path './.*/*')

test:
  <<: *test_anchor
  stage: test
  script:
    - pytest --cov-config=setup.cfg --cov skills/

analysis:
  <<: *test_anchor
  stage: test
  # The following pipe allows using a ':' in the 'sed' command, also remove the '-' list indicator
  script: |
    mkdir ./badges/
    radon cc -a . | tee ./badges/radon.log
    RCC_SCORE=$(sed -n 's/^Average complexity: \([A-F]\) .*/\1/p' ./badges/radon.log)
    anybadge --label=complexity --file=badges/rcc.svg --overwrite --value=$RCC_SCORE A=green B=yellow C=orange D=red E=red F=red
    echo "Radon cyclomatic complexity score is: $RCC_SCORE"
    pygount --folders-to-skip "venv,.*,node_modules" --format=summary ./
  artifacts:
    paths:
      - ./badges/

# ==================================================================================================
# ===== Stage: Container-Base ======================================================================

master_base_image_amd64:
  <<: *container_anchor
  stage: container_base
  variables:
    # Set base image variables here because it didn't work to set them in the anchor
    BASE_IMAGE_NAME: "master_base_image_amd64"
    BASE_IMAGE_NAME_FULL: "${CI_REGISTRY_IMAGE}/${BASE_IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
    IMAGE_FILE: "runfiles/Containerfile_MasterBase_amd64"
  script:
    # Build image, squash only new layers
    - echo "buildah bud --layers=false -f ${IMAGE_FILE} -t ${BASE_IMAGE_NAME} ."
    - buildah bud --layers=false -f ${IMAGE_FILE} -t ${BASE_IMAGE_NAME} .
    - buildah tag ${BASE_IMAGE_NAME} ${BASE_IMAGE_NAME_FULL}
    - buildah images
    # Push images
    - buildah push ${BASE_IMAGE_NAME_FULL}
  only:
    refs:
      # Run only for important branches or if the branch name starts with the 'containerbuild' keyword,
      # else every new branch would start to build this container
      - develop
      - release
      - /^containerbuild/

master_base_image_arm64:
  <<: *container_anchor
  stage: container_base
  variables:
    # Set base image variables here because it didn't work to set them in the anchor
    BASE_IMAGE_NAME: "master_base_image_arm64"
    BASE_IMAGE_NAME_FULL: "${CI_REGISTRY_IMAGE}/${BASE_IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
    IMAGE_FILE: "runfiles/Containerfile_MasterBase_arm64"
  script:
    # Build image, squash only new layers
    - echo "buildah bud --layers=false -f ${IMAGE_FILE} -t ${BASE_IMAGE_NAME} --platform=linux/arm64 ."
    - buildah bud --layers=false -f ${IMAGE_FILE} -t ${BASE_IMAGE_NAME} --platform=linux/arm64 .
    - buildah tag ${BASE_IMAGE_NAME} ${BASE_IMAGE_NAME_FULL}
    - buildah images
    # Push images
    - buildah push ${BASE_IMAGE_NAME_FULL}
  only:
    refs:
      # Run only for important branches or if the branch name starts with the 'containerbuild' keyword,
      # else every new branch would start to build this container
      - develop
      - release
      - /^containerbuild/

# ==================================================================================================
# ===== Stage: Container-Modules ===================================================================

mqtt_broker_amd64:
  <<: *container_anchor
  stage: container_modules
  needs:
    - master_base_image_amd64
  variables:
    BASE_IMAGE_NAME: "master_base_image_amd64"
    BASE_IMAGE_NAME_FULL: "${CI_REGISTRY_IMAGE}/${BASE_IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
    IMAGE_NAME: "mqtt_broker_amd64"
    IMAGE_FILE: "mqtt-broker/Containerfile_amd64"
    IMAGE_NAME_FULL: "${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
  script:
    # Download base image from selected branch and retag it
    - buildah pull ${BASE_IMAGE_NAME_FULL}
    - buildah tag ${BASE_IMAGE_NAME_FULL} ${BASE_IMAGE_NAME}
    # Build image, squash only new layers
    - echo "buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_NAME} ."
    - buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_NAME} .
    - buildah tag ${IMAGE_NAME} ${IMAGE_NAME_FULL}
    - buildah images
    # Push images
    - buildah push ${IMAGE_NAME_FULL}
  only:
    refs:
      # Run only for important branches or if the branch name starts with the 'containerbuild' keyword,
      # else every new branch would start to build this container
      - develop
      - release
      - /^containerbuild/

mqtt_broker_arm64:
  <<: *container_anchor
  stage: container_modules
  needs:
    - master_base_image_arm64
  variables:
    BASE_IMAGE_NAME: "master_base_image_arm64"
    BASE_IMAGE_NAME_FULL: "${CI_REGISTRY_IMAGE}/${BASE_IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
    IMAGE_NAME: "mqtt_broker_arm64"
    IMAGE_FILE: "mqtt-broker/Containerfile_arm64"
    IMAGE_NAME_FULL: "${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
  script:
    # Download base image from selected branch and retag it
    - buildah pull ${BASE_IMAGE_NAME_FULL}
    - buildah tag ${BASE_IMAGE_NAME_FULL} ${BASE_IMAGE_NAME}
    # Build image, squash only new layers
    - sed -i 's/FROM master_base_image_arm64/FROM localhost\/master_base_image_arm64/' mqtt-broker/Containerfile_arm64
    - echo "buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_NAME} --platform=linux/arm64 ."
    - buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_NAME} --platform=linux/arm64 .
    - buildah tag ${IMAGE_NAME} ${IMAGE_NAME_FULL}
    - buildah images
    # Push images
    - buildah push ${IMAGE_NAME_FULL}
  only:
    refs:
      # Run only for important branches or if the branch name starts with the 'containerbuild' keyword,
      # else every new branch would start to build this container
      - develop
      - release
      - /^containerbuild/

slu_parser_amd64:
  <<: *container_anchor
  stage: container_modules
  needs:
    - master_base_image_amd64
  when: manual
  variables:
    BASE_IMAGE_NAME: "master_base_image_amd64"
    BASE_IMAGE_NAME_FULL: "${CI_REGISTRY_IMAGE}/${BASE_IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
    IMAGE_NAME: "slu_parser_amd64"
    IMAGE_FILE: "slu-parser/Containerfile_amd64"
    IMAGE_NAME_FULL: "${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
  script:
    # Download base image from selected branch and retag it
    - buildah pull ${BASE_IMAGE_NAME_FULL}
    - buildah tag ${BASE_IMAGE_NAME_FULL} ${BASE_IMAGE_NAME}
    # Build image, squash only new layers
    - echo "buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_NAME} ."
    - buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_NAME} .
    - buildah tag ${IMAGE_NAME} ${IMAGE_NAME_FULL}
    - buildah images
    # Push images
    - buildah push ${IMAGE_NAME_FULL}
  only:
    refs:
      # Run only for important branches or if the branch name starts with the 'containerbuild' keyword,
      # else every new branch would start to build this container
      - develop
      - release
      - /^containerbuild/

slu_parser_arm64:
  <<: *container_anchor
  stage: container_modules
  needs:
    - master_base_image_arm64
  when: manual # gitlab-ci shared runners can't build it
  tags:
    - local
  variables:
    BASE_IMAGE_NAME: "master_base_image_arm64"
    BASE_IMAGE_NAME_FULL: "${CI_REGISTRY_IMAGE}/${BASE_IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
    IMAGE_NAME: "slu_parser_arm64"
    IMAGE_FILE: "slu-parser/Containerfile_arm64"
    IMAGE_NAME_FULL: "${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
  script:
    # Download base image from selected branch and retag it
    - buildah pull ${BASE_IMAGE_NAME_FULL}
    - buildah tag ${BASE_IMAGE_NAME_FULL} ${BASE_IMAGE_NAME}
    # Build image, squash only new layers
    - sed -i 's/FROM master_base_image_arm64/FROM localhost\/master_base_image_arm64/' slu-parser/Containerfile_arm64
    - echo "buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_NAME} --platform=linux/arm64 ."
    - buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_NAME} --platform=linux/arm64 .
    - buildah tag ${IMAGE_NAME} ${IMAGE_NAME_FULL}
    - buildah images
    # Push images
    - buildah push ${IMAGE_NAME_FULL}
  only:
    refs:
      # Run only for important branches or if the branch name starts with the 'containerbuild' keyword,
      # else every new branch would start to build this container
      - develop
      - release
      - /^containerbuild/

duckling_amd64:
  <<: *container_anchor
  stage: container_modules
  needs:
    - master_base_image_amd64
  when: manual
  variables:
    IMAGE_NAME: "duckling_amd64"
    IMAGE_FILE: "slu-parser/Containerfile_Duckling_amd64"
    IMAGE_NAME_FULL: "${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
  script:
    # Build image, squash only new layers
    - echo "buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_NAME} ."
    - buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_NAME} .
    - buildah tag ${IMAGE_NAME} ${IMAGE_NAME_FULL}
    - buildah images
    # Push images
    - buildah push ${IMAGE_NAME_FULL}
  only:
    refs:
      # Run only for important branches or if the branch name starts with the 'containerbuild' keyword,
      # else every new branch would start to build this container
      - develop
      - release
      - /^containerbuild/

duckling_arm64:
  <<: *container_anchor
  stage: container_modules
  needs:
    - master_base_image_arm64
  when: manual # gitlab-ci shared runners can't build it
  tags:
    - local
  variables:
    IMAGE_NAME: "duckling_arm64"
    IMAGE_FILE: "slu-parser/Containerfile_Duckling_arm64"
    IMAGE_NAME_FULL: "${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
  script:
    # Build image, squash only new layers
    - echo "buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_NAME} --platform=linux/arm64 ."
    - buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_NAME} --platform=linux/arm64 .
    - buildah tag ${IMAGE_NAME} ${IMAGE_NAME_FULL}
    - buildah images
    # Push images
    - buildah push ${IMAGE_NAME_FULL}
  only:
    refs:
      # Run only for important branches or if the branch name starts with the 'containerbuild' keyword,
      # else every new branch would start to build this container
      - develop
      - release
      - /^containerbuild/

text_to_speech_amd64:
  <<: *container_anchor
  stage: container_modules
  needs:
    - master_base_image_amd64
  variables:
    BASE_IMAGE_NAME: "master_base_image_amd64"
    BASE_IMAGE_NAME_FULL: "${CI_REGISTRY_IMAGE}/${BASE_IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
    IMAGE_NAME: "text_to_speech_amd64"
    IMAGE_FILE: "text-to-speech/Containerfile_amd64"
    IMAGE_NAME_FULL: "${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
  script:
    # Download base image from selected branch and retag it
    - buildah pull ${BASE_IMAGE_NAME_FULL}
    - buildah tag ${BASE_IMAGE_NAME_FULL} ${BASE_IMAGE_NAME}
    # Build image, squash only new layers
    - echo "buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_NAME} ."
    - buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_NAME} .
    - buildah tag ${IMAGE_NAME} ${IMAGE_NAME_FULL}
    - buildah images
    # Push images
    - buildah push ${IMAGE_NAME_FULL}
  only:
    refs:
      # Run only for important branches or if the branch name starts with the 'containerbuild' keyword,
      # else every new branch would start to build this container
      - develop
      - release
      - /^containerbuild/

text_to_speech_arm64:
  <<: *container_anchor
  stage: container_modules
  needs:
    - master_base_image_arm64
  variables:
    BASE_IMAGE_NAME: "master_base_image_arm64"
    BASE_IMAGE_NAME_FULL: "${CI_REGISTRY_IMAGE}/${BASE_IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
    IMAGE_NAME: "text_to_speech_arm64"
    IMAGE_FILE: "text-to-speech/Containerfile_arm64"
    IMAGE_NAME_FULL: "${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
  script:
    # Download base image from selected branch and retag it
    - buildah pull ${BASE_IMAGE_NAME_FULL}
    - buildah tag ${BASE_IMAGE_NAME_FULL} ${BASE_IMAGE_NAME}
    # Build image, squash only new layers
    - sed -i 's/FROM master_base_image_arm64/FROM localhost\/master_base_image_arm64/' text-to-speech/Containerfile_arm64
    - echo "buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_NAME} --platform=linux/arm64 ."
    - buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_NAME} --platform=linux/arm64 .
    - buildah tag ${IMAGE_NAME} ${IMAGE_NAME_FULL}
    - buildah images
    # Push images
    - buildah push ${IMAGE_NAME_FULL}
  only:
    refs:
      # Run only for important branches or if the branch name starts with the 'containerbuild' keyword,
      # else every new branch would start to build this container
      - develop
      - release
      - /^containerbuild/

# ==================================================================================================
# ===== Stage: Release =============================================================================

publish_test_image_to_master:
  # Download images from release branch and push them to master branch
  <<: *container_anchor
  stage: release
  when: manual
  variables:
    IMAGE_NAME: "testing_jaco_mtr"
    IMAGE_NAME_FULL: "${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}"
    IMAGE_NAME_TARGET: "${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:master"
  script:
    - buildah pull ${IMAGE_NAME_FULL}
    - buildah tag ${IMAGE_NAME_FULL} ${IMAGE_NAME_TARGET}
    - buildah push ${IMAGE_NAME_TARGET}
  only:
    refs:
      - release

publish_base_images_to_master:
  # Download images from release branch and push them to master branch
  <<: *container_anchor
  stage: release
  when: manual
  variables:
    IMAGE_AMD64: "master_base_image_amd64"
    IMAGE_ARM64: "master_base_image_arm64"
    IMAGE_AMD64_FULL: "${CI_REGISTRY_IMAGE}/${IMAGE_AMD64}:${CI_COMMIT_REF_SLUG}"
    IMAGE_ARM64_FULL: "${CI_REGISTRY_IMAGE}/${IMAGE_ARM64}:${CI_COMMIT_REF_SLUG}"
    IMAGE_AMD64_TARGET: "${CI_REGISTRY_IMAGE}/${IMAGE_AMD64}:master"
    IMAGE_ARM64_TARGET: "${CI_REGISTRY_IMAGE}/${IMAGE_ARM64}:master"
  script:
    - buildah pull ${IMAGE_AMD64_FULL}
    - buildah pull ${IMAGE_ARM64_FULL}
    - buildah tag ${IMAGE_AMD64_FULL} ${IMAGE_AMD64_TARGET}
    - buildah tag ${IMAGE_ARM64_FULL} ${IMAGE_ARM64_TARGET}
    - buildah push ${IMAGE_AMD64_TARGET}
    - buildah push ${IMAGE_ARM64_TARGET}
  only:
    refs:
      - release

publish_mqtt_broker_to_master:
  # Download images from release branch and push them to master branch
  <<: *container_anchor
  stage: release
  when: manual
  variables:
    IMAGE_AMD64: "mqtt_broker_amd64"
    IMAGE_ARM64: "mqtt_broker_arm64"
    IMAGE_AMD64_FULL: "${CI_REGISTRY_IMAGE}/${IMAGE_AMD64}:${CI_COMMIT_REF_SLUG}"
    IMAGE_ARM64_FULL: "${CI_REGISTRY_IMAGE}/${IMAGE_ARM64}:${CI_COMMIT_REF_SLUG}"
    IMAGE_AMD64_TARGET: "${CI_REGISTRY_IMAGE}/${IMAGE_AMD64}:master"
    IMAGE_ARM64_TARGET: "${CI_REGISTRY_IMAGE}/${IMAGE_ARM64}:master"
  script:
    - buildah pull ${IMAGE_AMD64_FULL}
    - buildah pull ${IMAGE_ARM64_FULL}
    - buildah tag ${IMAGE_AMD64_FULL} ${IMAGE_AMD64_TARGET}
    - buildah tag ${IMAGE_ARM64_FULL} ${IMAGE_ARM64_TARGET}
    - buildah push ${IMAGE_AMD64_TARGET}
    - buildah push ${IMAGE_ARM64_TARGET}
  only:
    refs:
      - release

publish_slu_parser_to_master:
  # Download images from release branch and push them to master branch
  <<: *container_anchor
  stage: release
  when: manual
  variables:
    IMAGE_AMD64: "slu_parser_amd64"
    IMAGE_ARM64: "slu_parser_arm64"
    IMAGE_AMD64_FULL: "${CI_REGISTRY_IMAGE}/${IMAGE_AMD64}:${CI_COMMIT_REF_SLUG}"
    IMAGE_ARM64_FULL: "${CI_REGISTRY_IMAGE}/${IMAGE_ARM64}:${CI_COMMIT_REF_SLUG}"
    IMAGE_AMD64_TARGET: "${CI_REGISTRY_IMAGE}/${IMAGE_AMD64}:master"
    IMAGE_ARM64_TARGET: "${CI_REGISTRY_IMAGE}/${IMAGE_ARM64}:master"
  script:
    - buildah pull ${IMAGE_AMD64_FULL}
    - buildah pull ${IMAGE_ARM64_FULL}
    - buildah tag ${IMAGE_AMD64_FULL} ${IMAGE_AMD64_TARGET}
    - buildah tag ${IMAGE_ARM64_FULL} ${IMAGE_ARM64_TARGET}
    - buildah push ${IMAGE_AMD64_TARGET}
    - buildah push ${IMAGE_ARM64_TARGET}
  only:
    refs:
      - release

publish_duckling_to_master:
  # Download images from release branch and push them to master branch
  <<: *container_anchor
  stage: release
  when: manual
  variables:
    IMAGE_AMD64: "duckling_amd64"
    IMAGE_ARM64: "duckling_arm64"
    IMAGE_AMD64_FULL: "${CI_REGISTRY_IMAGE}/${IMAGE_AMD64}:${CI_COMMIT_REF_SLUG}"
    IMAGE_ARM64_FULL: "${CI_REGISTRY_IMAGE}/${IMAGE_ARM64}:${CI_COMMIT_REF_SLUG}"
    IMAGE_AMD64_TARGET: "${CI_REGISTRY_IMAGE}/${IMAGE_AMD64}:master"
    IMAGE_ARM64_TARGET: "${CI_REGISTRY_IMAGE}/${IMAGE_ARM64}:master"
  script:
    - buildah pull ${IMAGE_AMD64_FULL}
    - buildah pull ${IMAGE_ARM64_FULL}
    - buildah tag ${IMAGE_AMD64_FULL} ${IMAGE_AMD64_TARGET}
    - buildah tag ${IMAGE_ARM64_FULL} ${IMAGE_ARM64_TARGET}
    - buildah push ${IMAGE_AMD64_TARGET}
    - buildah push ${IMAGE_ARM64_TARGET}
  only:
    refs:
      - release

publish_tts_to_master:
  # Download images from release branch and push them to master branch
  <<: *container_anchor
  stage: release
  when: manual
  variables:
    IMAGE_AMD64: "text_to_speech_amd64"
    IMAGE_ARM64: "text_to_speech_arm64"
    IMAGE_AMD64_FULL: "${CI_REGISTRY_IMAGE}/${IMAGE_AMD64}:${CI_COMMIT_REF_SLUG}"
    IMAGE_ARM64_FULL: "${CI_REGISTRY_IMAGE}/${IMAGE_ARM64}:${CI_COMMIT_REF_SLUG}"
    IMAGE_AMD64_TARGET: "${CI_REGISTRY_IMAGE}/${IMAGE_AMD64}:master"
    IMAGE_ARM64_TARGET: "${CI_REGISTRY_IMAGE}/${IMAGE_ARM64}:master"
  script:
    - buildah pull ${IMAGE_AMD64_FULL}
    - buildah pull ${IMAGE_ARM64_FULL}
    - buildah tag ${IMAGE_AMD64_FULL} ${IMAGE_AMD64_TARGET}
    - buildah tag ${IMAGE_ARM64_FULL} ${IMAGE_ARM64_TARGET}
    - buildah push ${IMAGE_AMD64_TARGET}
    - buildah push ${IMAGE_ARM64_TARGET}
  only:
    refs:
      - release
