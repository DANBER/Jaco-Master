# Jaco-Master

Jaco is an offline running privacy-aware Voice Assistant. It is completely open source and supports multiple languages.

In an open Skill Store you can share your own skills or download skills from other developers. \
(The store can be found [here](https://jaco-store.web.app/))

Using automatically generated domain specific language models Jaco performs better than widely used alternatives. \
(You can find the benchmarks under this [link](https://gitlab.com/Jaco-Assistant/Benchmark-Jaco))

An overview of the general approach can be found in the paper [Jaco: An Offline Running Privacy-aware Voice Assistant](https://arxiv.org/pdf/2209.07775.pdf).

<div align="center">
    <img src="media/jaco.png" alt="jaco logo" width="75%"/>
</div>

<br/>

[![pipeline status](https://gitlab.com/Jaco-Assistant/Jaco-Master/badges/master/pipeline.svg)](https://gitlab.com/Jaco-Assistant/Jaco-Master/-/commits/master)
[![code style black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![code complexity](https://gitlab.com/Jaco-Assistant/Jaco-Master/-/jobs/artifacts/master/raw/badges/rcc.svg?job=analysis)](https://gitlab.com/Jaco-Assistant/Jaco-Master/-/commits/master)

<br/>

### Language Support

Currently the supported languages are: \
(With the word error rate of free speech-to-text transcription on the challenging [CommonVoice](https://commonvoice.mozilla.org/) dataset,
which is a very important factor to estimate the overall spoken command acceptance accuracy.)

- German (de): 4.0%
- English (en): 9.1%
- Spanish (es): 5.7%
- French (fr): 8.1%

<br/>

<div align="center">
    <img src="media/benefits-slide.png" alt="jaco benefits" width="75%"/>
</div>

<br/>

### Important updates

**21.01.2023**

- Skills can now request arbitrary text inputs with the _greedy_text_ intent.
- _Breaking change_: Recreate topic-keys and copy them to the _satellites_ again.

**15.01.2023**

- Updated _Satellite's audio-streamer_, which now has a simpler setup and less software dependencies, but at the same time allows more audio options.
- _Breaking change_: Install _Jaco-Satellite_ again.

**27.11.2022**

- Improved release workflow.

**16.09.2022**

- Updated STT model to _conformer_ architecture. Jaco now reaches _state-of-the-art_ performance in most _Speech-to-Intent_ benchmarks. Checkout [finstreder's paper](https://arxiv.org/pdf/2206.14589.pdf) for more details.
- _Breaking change_: Run installation commands again.

**07.03.2022**

- Switched architecture on Raspberry-Pi to _arm64_ for improved performance. Thanks @alexkn.
- _Breaking change_: Jaco-Master now requires a 64-bit OS on Raspberry-Pi.

**22.02.2022**

- Improved reaction times.

**10.08.2021**

- Integration of _finstreder_ for much faster SLU model training and improved command recognition accuracy. This also replaces _rasa_ as NLU service.
- Replaced _podman_ with _docker_, because _podman_ dropped support for raspbian systems.
- Integration of _portainer_ to simplify container handling.
- Using master-base-image for skills too.
- _Breaking changes_: Many. Install docker tools and update all containers.

<br/>

## Setup

Because Jaco is using containers it should run on almost any computer with one of `amd64,arm64` architectures, the Satellite also supports `armhf`.
Setup is tested for Linux-Ubuntu computers and a Raspberry-Pi 4 with 4GB memory (Requires Raspbian-Buster or newer).
The complete installation takes about 30min on a computer and about 1h on a raspi. About 4GB disk space is required.

- Clone this repository (note the extra flag which initializes the _jacolib_ submodule):

  ```bash
  git clone --recurse-submodules https://gitlab.com/Jaco-Assistant/Jaco-Master.git
  cd Jaco-Master/
  ```

- Install [docker](https://docs.docker.com/engine/install/) on your master device.
  Afterwards enable [rootless usage](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user) of docker for increased convenience (else you would need to prefix all `docker build/run` commands with `sudo`).

  Check the installation by running `docker run hello-world` and `docker run --rm -it ubuntu:20.04`.
  If this doesn't work, your installation is broken, please google for solutions.
  Also test `docker buildx build --help`, if it's not working, update your docker installation.

- Install required python libraries:

  ```bash
  sudo apt-get install -y python3-pip
  sudo pip3 install --upgrade pyyaml docker-compose
  ```

- Adjust `userdata/config/global_config.template.yaml` to your needs and save as `userdata/config/global_config.yaml`. \
   (See mqtt-broker readme for instructions to change mqtt authentication, but first execute the next step)

- Install modules: \
  (This will download the prebuilt images, about 2.5GB for a computer or 3GB for a Raspberry Pi. The download itself has approximately one third of the size since the images are compressed.)

  ```bash
  python3 runfiles/install.py --install_modules
  ```

- Choose some skills in the [Skill-Store](https://jaco-store.web.app/). \
  Copy the exported skill links into `userdata/my_selected_skills.txt`. \
  (You can overwrite the demo skill link if you like. Alternatively you can skip this step and just install the demo skill) \
  Then update the skills:

  ```bash
  python3 runfiles/install.py --update_skills
  ```

- Install [Jaco-Satellite](https://gitlab.com/Jaco-Assistant/Jaco-Satellite) on your master device or all your satellite devices. \
  Copy `userdata/module_topic_keys.json` to all satellite's userdata directories.

- Start modules and skills:

  ```bash
  docker-compose -f userdata/start-modules-compose.yml up

  # Wait until the mqtt-broker container was started and then run in a new terminal:
  docker-compose -f userdata/start-skills-compose.yml up
  ```

- Start satellite modules (See Jaco-Satellite readme): \
  Wait until all modules are ready before starting interaction (Normally the last message is that the nlu-parser connected to the mqtt-broker)

- Stop modules:

  ```bash
  docker-compose -f userdata/start-modules-compose.yml down
  docker-compose -f userdata/start-skills-compose.yml down
  ```

If something isn't working, please look at the readme files in the different modules for further debugging instructions, or check the source code.

<br/>

If everything did work, you can run Jaco in the background or enable it to start on bootup.

- To run it in the background, add the `--detach` flag at the end of the `docker-compose ... up` commands:

  ```bash
  docker-compose -f userdata/start-modules-compose.yml up --detach
  sleep 60; docker-compose -f userdata/start-skills-compose.yml up --detach
  ```

- To enable starting on bootup, edit `userdata/start-jaco-master-modules.template.service` and `start-jaco-master-skills.template.service`.
  As before save the files without the `.template` extension. Then enable and start the services:

  ```bash
  # Master Modules
  sudo systemctl enable `pwd`/userdata/start-jaco-master-modules.service
  sudo systemctl start start-jaco-master-modules.service

  # Check Logs
  systemctl status start-jaco-master-modules.service
  journalctl -u start-jaco-master-modules.service -e -f -b

  # Skills
  sudo systemctl enable `pwd`/userdata/start-jaco-master-skills.service
  sudo systemctl start start-jaco-master-skills.service

  # Stop or remove with
  sudo systemctl stop start-jaco-master-modules.service
  sudo systemctl disable start-jaco-master-modules.service

  # Update service after file changes
  sudo systemctl daemon-reload
  ```

- If something doesn't work now, see debugging section for how to print debug logs or
  stop the services (all or only one or two) and use the `docker-compose up/down` commands to debug the error.
  (You get more status outputs there and I haven't found a way to view them in the service logs)

<br/>

## Updating

To update the skills or install new ones run: \
(Before running this on a raspberry with only 1gb ram, stop the other modules first. You can use the `docker-compose down` commands for this.)

```bash
python3 runfiles/install.py --update_skills
```

To update the module code and containers run:

```bash
git pull && git submodule update --recursive
python3 runfiles/install.py --download_modules
```

To update everything just repeat the setup steps.

<br/>

## How does a Voice Assistant work?

<div align="center">
    <img src="media/jaco_flow.png" alt="jaco workflow" width="75%"/>
</div>

A Voice Assistant normally consists out of a few modules with special tasks:
A wake word engine listens to the user to say the wake word. Then the following voice command is send to a speech to text module.
The detected text is now used by natural language understanding engine which extracts its intent (meaning).
The intent will be send to a skill which can handle this specific task. The skill can then send a response to the user,
which is converted to speech by the text to speech module.
You can find more information about how Jaco works in the paper: [Jaco: An Offline Running Privacy-aware Voice Assistant](https://dl.acm.org/doi/pdf/10.5555/3523760.3523842).

<br/>

<div align="center">
    <img src="media/skill-slide.jpg" alt="jaco skills" width="95%"/>
</div>

Building your own skill for _Jaco_ is really simple and requires only a few steps, most importantly defining voice commands and implementing some skill logic. You can also find a more detailed implementation example at the demo skill: [Skill-Riddles](https://gitlab.com/Jaco-Assistant/Skill-Riddles).

<br/>

## Contributing

You can contribute to this project in multiple ways:

- Help to solve the open issues

- Develop new skills or translate already exiting ones (See [Skill-Riddles](https://gitlab.com/Jaco-Assistant/Skill-Riddles) for further instructions)

- Add a new language:
  - Train a [Scribosermo](https://gitlab.com/Jaco-Assistant/Scribosermo) model or find a download link to one \
    (Alternatively you can also replace the _slu-parser_ or its _speech-to-text_ library with a different model)
  - Extend `model_params.json` and `langdicts.json` dictionaries in [slu-parser](slu-parser/moduldata/)
  - Translate _Skill-Dialogs_ and _Skill-Riddles_
  - Add language to dropdown in the _Skill-Store_
  - Update language support in this readme
- Help improving the speech recognition accuracy by contributing to the [CommonVoice](https://voice.mozilla.org)-Project

<br/>

## Citation

Please cite _Jaco_ if you found it helpful for your research or business.

```bibtex
@inproceedings{
  jaco,
  title={Jaco: An Offline Running Privacy-aware Voice Assistant},
  author={Bermuth, Daniel and Poeppel, Alexander and Reif, Wolfgang},
  booktitle={Proceedings of the 2022 ACM/IEEE International Conference on Human-Robot Interaction},
  pages={618--622},
  year={2022}
}
```

<br/>

## Alternative Modules

- GameTTS, high quality german voices: https://gitlab.com/DANBER/Jaco-GameTTS \
  (Won't run on RaspberryPi)

<br/>

## Debugging

Check the instructions in the readme file of the different modules. \
They contain everything which is related to their specific tasks.

<br/>

Checkout container logs or restart single containers at: http://localhost:9000/ \
After opening the link the first time, set some password and select docker in the next step.

Change the the log level of the autostart services with:

```bash
systemd-analyze get-log-level
sudo systemd-analyze set-log-level debug
```

Build base container:

```bash
docker build -t master_base_image_amd64 - < runfiles/Containerfile_MasterBase_amd64
```

Enable cross-building for other architectures: \
(You have to rerun the first command after every restart of your computer)

```bash
sudo docker run --security-opt label=disable --rm --privileged multiarch/qemu-user-static --reset -p yes
docker buildx build --platform=linux/arm64 -t master_base_image_arm64 - < runfiles/Containerfile_MasterBase_arm64
```

Build all images:
(Building the amd64 images on a modern computer requires 15GB free space and takes about 30&thinsp;min, or about 8&thinsp;h and at least 32GB RAM for the arm64 images)

```bash
# To squash the images, experimental features are required
sudo nano /etc/docker/daemon.json
# Insert
{
  "experimental": true
}
# Restart docker and check
sudo service docker restart
docker version

python3 runfiles/install.py --build_modules --architecture amd64
```

Upload prebuilt arm64 images: \
(If you get an 'invalid status code' error with some of them images, rerun the script again)

```bash
./runfiles/publish-images.sh
```

Docker image commands:

```bash
# Show all images
docker image ls

# Remove broken images and free some space
docker system prune

# Remove single image
docker image rm -f {Image-ID}

# Remove all images
docker rm -vf $(docker ps -a -q)
docker rmi -f $(docker images -a -q)
```

Run tests: \
See [this readme](tests/README.md)

Update jacolib's submodule commit:

```bash
git submodule update --remote --merge
```

## CI Pipelines

### Cleanup Container Registry

The CI Pipeline pushes images with an image tag per git branch. \
If you use multiple branches for development you should consider using gitlab image tag cleanup (Project->Settings->Packages & Registries) to automatically delete old tags.

### Gitlab CI local runner

_slu_parser_arm64_ and _duckling_arm64_ can't be build by gitlab shared runners because these jobs exceed the 3h timeout. \
You can use a local runner to build them.

[Install a local runner](https://docs.gitlab.com/runner/install/docker.html) and [register it](https://docs.gitlab.com/runner/register/index.html#docker) with the tag "local".

Enable cross-building for other architectures: \
(You have to rerun the command after every restart of your computer)

```bash
sudo docker run --security-opt label=disable --rm --privileged multiarch/qemu-user-static --reset -p yes
```

buildah needs [some configuration](https://github.com/containers/buildah/tree/main/contrib/buildahimage) to run, so you have to edit the _config.toml_ file manually. \
Add the following lines to the \[runners.docker\] section:

```
[runners.docker]
security_opt = ["seccomp:unconfined","apparmor:unconfined"]
devices = ["/dev/fuse"]
```

When your runner is started, you can start these jobs via the play button in the gitlab pipeline overview. \
You can stop the runner if it's not needed.
